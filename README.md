# README #

How vuw animations work.

### What will i learn? ###

* how Vue transition tag works
* How the types of transition classes work: v-enter, v-enter-active, v-leave, v-leave-active
* To use the transition tag property "type" to set what king of ccs class it will take (animation or transition)
* How overrite the vue transition classes by default
* how setup the transition to appear from the inital render (when the page load) using: appear
* switch transitions (slide and fade)
* Javascript animations

### Docs ###

* https://es.vuejs.org/v2/guide/transitions.html
